//
//  KeyboardViewController.swift
//  APCustomKeyboard
//
//  Created by Phu Quang Nguyen on 1/20/16.
//  Copyright © 2016 Phu Quang Nguyen. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!

    private var rowCount: Int = 3
    private let columnCount: Int = 8
    private let itemCount: Int = 50
    private var pageCount: Int = 0
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    
        let nib = UINib(nibName: "KeyboardView", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        view = objects[0] as! UIView
        
//        let heightConstraint = NSLayoutConstraint(item: self.view, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 0.0, constant: 300)
//        self.view.addConstraint(heightConstraint)
        
        scrollView.delegate = self
        pageCount = Int(ceil(Float(itemCount) / Float(rowCount * columnCount)))
        
        pageControl.numberOfPages = pageCount
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        
        /*if (toInterfaceOrientation.isLandscape) {
            rowCount = 2
        }
        else {
            rowCount = 3
        }
        
        pageCount = Int(ceil(Float(itemCount) / Float(rowCount * columnCount)))
        pageControl.numberOfPages = pageCount
        
//        self.view.layoutIfNeeded()
//        self.view.setNeedsDisplay()
        
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        print("scrollView bounds: \(screenWidth) \(screenHeight)")
        
        let pageWidth = screenWidth
        let pageHeight = screenHeight - 100
        
        for subView in scrollView.subviews {
            subView.removeFromSuperview()
        }
        
        scrollView.contentSize = CGSizeMake(CGFloat(pageCount) * pageWidth, pageHeight)
        
        for index in 0..<Int(pageCount) {
            let subView = UIView(frame: CGRectMake(CGFloat(index) * pageWidth, 0, pageWidth, pageHeight))
            addItemsForView(subView, atIndex: index)
            scrollView.addSubview(subView)
        }*/
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let pageWidth = scrollView.bounds.width
        let pageHeight = scrollView.bounds.height
        print("scrollView bounds: \(pageWidth) \(pageHeight)")
        
        scrollView.contentSize = CGSizeMake(CGFloat(pageCount) * pageWidth, pageHeight)
        
        for index in 0..<Int(pageCount) {
            let subView = UIView(frame: CGRectMake(CGFloat(index) * pageWidth, 0, pageWidth, pageHeight))
            subView.translatesAutoresizingMaskIntoConstraints = false
            addItemsForView(subView, atIndex: index)
            scrollView.addSubview(subView)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let screenHeight = UIScreen.mainScreen().bounds.size.height
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        print("scrollView bounds: \(screenWidth) \(screenHeight)")
    }
    
    private func addItemsForView(view: UIView, atIndex index: Int) {
        let itemWidth: CGFloat = view.bounds.size.width / CGFloat(columnCount)
        let itemHeight: CGFloat = view.bounds.size.height / CGFloat(rowCount)
        var numberOfItems = columnCount * rowCount

        if index == pageCount - 1 {
            numberOfItems = itemCount - index * numberOfItems
        }
        
        let numberOfRows = Int(ceil(Float(numberOfItems) / Float(columnCount)))
        
        for rowIndex in 0..<numberOfRows {
            var numberOfColumns = columnCount
            if rowIndex == numberOfRows - 1 {
                numberOfColumns = numberOfItems - rowIndex * numberOfColumns
            }
            for columnIndex in 0..<numberOfColumns {
                let button = UIButton(frame: CGRectMake(CGFloat(columnIndex) * itemWidth, CGFloat(rowIndex) * itemHeight, itemWidth, itemHeight))
                button.setImage(UIImage(named: "ico_furious"), forState: .Normal)
                view.addSubview(button)
            }
        }
    }

    // MARK: - Action
    
    @IBAction func nextKeyboard(sender: UIButton) {
        advanceToNextInputMode()
    }

    @IBAction func furiousIcon(sender: UIButton) {
        
    }
    
    @IBAction func smileIcon(sender: UIButton) {
        
    }
    
    @IBAction func share(sender: UIButton) {
        
    }
    
    @IBAction func pageControlChanged(sender: UIPageControl) {
        let page = sender.currentPage
        var frame = scrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        scrollView.scrollRectToVisible(frame, animated: true)
    }

}

extension KeyboardViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let pageWidth:CGFloat = CGRectGetWidth(scrollView.bounds)
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);
    }
    
}
